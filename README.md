 
 
 # Linux Package Helper for Begginers
 
 **All avilable Linux distro:** *Arch Linux*, *Manjaro Linux*
 
 **Linux distributions soon:** *Debian* 

  <a href="#installation">Installation</a>
  •
  <a href="#install-needed-packages">Install needed packages</a>
  •
  <a href="#package-description">Package description</a>
</p>

![Image](https://i.imgur.com/4Kh5h3u.png)

 # Installation

### First way

1. You need to download release from [here](https://github.com/naddurkostia/LinuxPkg-Helper/releases/tag/Linux)
2. Open terminal and move to the folder you just downloaded and unzipped
3. After this, you need to write `makepkg -si`
4. Now you can run the script using the `pkg-helper` command

### **Done.**


 # Install needed packages.

**You need to select needed packages that you want to install.**

<br/>

![Image](https://i.imgur.com/Px7OjRx.png)

# Script control

**🠔 🠖** - these buttons select OK or Cancel

**🠕 🠗** - these buttons select packages

<br/>

# Package description

### Important PKG - all important packages that you need to  install on your Linux distro

<br/>

### Gaming PKG - important packages for gaming on the Linux

<br/>

### Programming PKG - all packages for convenient programming.

<br/>

# Contact me

[![Telegram](https://img.shields.io/badge/-Telegram-090909?style=for-the-badge&logo=telegram&logoColor=27A0D9)](https://t.me/kostiandd)
[![Instagram](https://img.shields.io/badge/-Instagram-090909?style=for-the-badge&logo=instagram&logoColor=B4068E)](https://www.instagram.com/nadduur/)
[![Discord](https://img.shields.io/badge/-Discord-090909?style=for-the-badge&logo=discord&logoColor=4d92e1)](https://discord.gg/8mzN3UFW)
[![Twitter](https://img.shields.io/badge/-Twitter-090909?style=for-the-badge&logo=Twitter&logoColor=1C9DEB)](https://twitter.com/naddurkostia)

---

### Repository will be supplementing.








